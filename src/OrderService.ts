import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from "./dto/CreateOrderDto";
import { OrderRepository } from "./OrderRepository";
import { Order } from "./entity/Order";
import { OrderItem } from "./entity/OrderItem";

@Injectable()
export class OrderService {

  constructor(
      private readonly orderRepository: OrderRepository
  ) {}

  public async create(data: CreateOrderDto): Promise<Order> {
    const order = new Order();
    order.name = data.name;

    order.items = data.items.map(content => {
      const item = new OrderItem();
      item.content = content;
      return item;
    });

    await this.orderRepository.save(order);

    return order;
  }
}

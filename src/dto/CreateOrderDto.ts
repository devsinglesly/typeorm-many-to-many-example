/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class CreateOrderDto
 */
export class CreateOrderDto {
    public name: string;
    public items: string[];
}

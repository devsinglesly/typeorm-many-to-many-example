/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Order
 */
import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { OrderItem } from "./OrderItem";

@Entity({name: 'orders'})
export class Order {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @ManyToMany(() => OrderItem, {
        cascade: true,
    })
    @JoinTable()
    public items: OrderItem[];
}

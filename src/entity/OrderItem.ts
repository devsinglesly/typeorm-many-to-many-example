/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class OrderItem
 */
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "order_items"})
export class OrderItem {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public content: string;

}

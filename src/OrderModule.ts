import { Module, OnApplicationBootstrap } from '@nestjs/common'
import { TypeOrmModule } from "@nestjs/typeorm";
import { Order } from "./entity/Order";
import { OrderItem } from "./entity/OrderItem";
import { OrderRepository } from "./OrderRepository";
import { OrderService } from "./OrderService";
import { CreateOrderDto } from "./dto/CreateOrderDto";

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '0.0.0.0',
      port: 3307,
      username: 'root',
      password: '1234',
      database: 'test',
      entities: [
          Order,
          OrderItem
      ],
      synchronize: true,
    }),
  ],
  controllers: [],
  providers: [
      OrderRepository,
      OrderService
  ],
})
export class OrderModule implements OnApplicationBootstrap {

    constructor(
        private readonly orderService: OrderService
    ) {}

    public async onApplicationBootstrap(): Promise<void> {
        const input = {
            name: "Order 1",
            items: [
                "AMAZON",
                "GOOGL",
                "XMR"
            ]
        } as CreateOrderDto;

        await this.orderService.create(input);
    }
}

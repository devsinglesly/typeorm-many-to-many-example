/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class OrderRepository
 */
import { Connection, Repository } from "typeorm";
import { Order } from "./entity/Order";
import { Injectable } from "@nestjs/common";

@Injectable()
export class OrderRepository {

    private readonly repository: Repository<Order> = this.connection.getRepository(Order);

    public constructor(
        private readonly connection: Connection
    ) {}

    public async save(order: Order): Promise<void> {
        await this.repository.save(order);
    }
}

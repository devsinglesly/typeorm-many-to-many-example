import { NestFactory } from '@nestjs/core';
import { OrderModule } from './OrderModule';

(async () => (await NestFactory.create(OrderModule)).listen(3000))();
